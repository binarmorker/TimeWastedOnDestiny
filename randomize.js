// To be inserted in console to randomize data (for screenshots or demos)

const randomNames = [
  'Glamorous',
  'Ferocious',
  'SpookyClever',
  'ItchyDraugr',
  'VacationHard',
  'RubidiumToad',
  'HollowBerget',
  'YellowString',
  'TroubledRed',
  'RagdollDate',
  'Mothman the Goldina',
  'Minotaur the Amien',
  'Reveena the Kelpie',
  'Ghost the Famous',
  'Werewolf the Yeti',
  'Cupid Wolpertinger the Dull',
  'Sprite the Little',
  'Owlman the Outrageous',
  'Sphinx Daemon the Fantastic',
  'Elemental Pontianak the Nice',
  'Fl00rOny3k4chukw69',
  'DybbukFurgus0n420',
  '420J0sh4u4H4rsh',
  '420Sw33tT4sty',
  'Sh4m3fulB0k420',
  '4dv3ntur0usB4d69',
  'G4rg4ntu4nBh4r4th420',
  '420P3rfum3dF41nt',
  '420Gr13fT34m',
  '4204nx10usF4bul0us',
  'XxExaminationxX',
  'XxMaxParimalxX',
  'XxSevgulShepxX',
  'XxTizherukPeaxX',
  'XxGreatDraugrxX',
  'XxDramaticPigxX',
  'XxHoldKipxX',
  'XxDealerMemexX',
  'XxRockhopperxX',
  'XxZealousCornxX',
  'Dr4ftyM4n0ch3hr',
  'Gr4p3B1lb1l',
  'M4ur1z14S0ph1st1c4t3d',
  'T3rr1bl3T4sty',
  '3ng1n3Cr4b',
  'C4ul1fl0w3rM4nd4r1n',
  'C4tAd34',
  'DryR4d1um',
  'F3r4lC4ndy',
  'S3xySup3r'
]

const randomNumber = (max, min = 0) => Math.floor(Math.random() * max) + min

const randomize = () => {
  document.querySelectorAll('.player a').forEach(x => { x.innerText = randomNames[randomNumber(randomNames.length)] }) // Random names
  document.querySelectorAll('.player .time-played').forEach(x => { x.innerText = `${randomNumber(10_000, 10_000)}h ${randomNumber(59, 1)}m` }) // Random time played
  document.querySelectorAll('.player').forEach(x => { x.style.filter = 'blur(3px)' }) // Sets blur
}

randomize()
